<?php


namespace ShopExpress\ShopCrmSyncClient\Entity;


/**
 * Class DeliveryTypeEntity
 * @package ShopExpress\ShopCrmSyncClient\Entity
 */
class DeliveryTypeEntity extends AbstractEntity
{
    /**
     * @var string
     */
    public static $tableName = 'delivery_types';

    /**
     * @var array
     */
    protected $requiredFields = [
        'sync_id',
    ];
}