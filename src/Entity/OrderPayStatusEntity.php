<?php


namespace ShopExpress\ShopCrmSyncClient\Entity;


/**
 * Class OrderPayStatusEntity
 * @package ShopExpress\ShopCrmSyncClient\Entity
 */
class OrderPayStatusEntity extends AbstractEntity
{
    /**
     * @var string
     */
    public static $tableName = 'order_pay_statuses';

    /**
     * @var array
     */
    protected $requiredFields = [
        'sync_id',
    ];
}