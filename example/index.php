<?php

if (!file_exists(__DIR__ . '/.env')) {
    exit(".env file does not exists. Please, run composer install command.");
}

$config = parse_ini_file(__DIR__ . '/.env');

require 'vendor/autoload.php';

use ShopExpress\ShopCrmSyncClient\ApiClient;
use ShopExpress\ShopCrmSyncClient\Entity\SyncEntity;
use ShopExpress\ShopCrmSyncClient\Entity\SyncSettingEntity;

try {
    $client = new ApiClient($config['BASE_URL'], $config['API_TOKEN']);
    
    // Создание синхронизации

    /* @var $entity SyncEntity */
    $entity = $client->createEntity(
        SyncEntity::CLASS,
        [
            'user_id' => 1,
            'shop_url' => 'http://shop.ru',
            'shop_apikey' => '12345',
            'crm_url' => 'http://retailcrm.ru/myshop',
            'crm_apikey' => '12345',
            'shop_type' => 'ShopExpress',
            'crm_type' => 'RetailCrm',
        ]
    );

    // Обновление синхронизации
    $entity['shop_apikey'] = '3456789';
    $entity->save();

    // Получение всех параметров для синхронизации
    $settings = $entity->getSettings();

    // Получение связей способов доставки
    $settings = $entity->getDeliveryTypes();

    // Обновление параметра

    /* @var $setting SyncSettingEntity */
    $setting = $settings[0];
    $setting['value'] = 'test_value';
    $setting->save();

    // Удаление синхронизации
    $entity->delete();

    // Получение одного параметра
    $setting = $client->getEntity(SyncSettingEntity::CLASS, ['id' => 1]);

    // Получение всех синхронизаций для указанного пользователя
    $entities = $client->getAllEntity(SyncEntity::CLASS, ['user_id' => 1]);
} catch (Throwable $e) {
    echo $e->getMessage();
    echo $e->getTraceAsString();
}