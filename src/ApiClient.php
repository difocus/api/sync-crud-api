<?php

namespace ShopExpress\ShopCrmSyncClient;

use Exception;
use ShopExpress\ShopCrmSyncClient\Entity\AbstractEntity;
use ShopExpress\ShopCrmSyncClient\Http\Client;
use ShopExpress\ShopCrmSyncClient\Injector\LoggerInjector;

/**
 * Class ApiClient
 * @package ShopExpress\ShopCrmSyncClient
 */
class ApiClient
{
    use LoggerInjector;

    /**
     * @var Client
     */
    private $client;

    /**
     * @param string $baseUrl The base url
     * @param string $token
     */
    public function __construct(string $baseUrl, string $token)
    {
        $this->client = new Client($baseUrl, compact('token'));
    }

    /**
     * @param string $classEntity
     * @param array $data
     *
     * @throws Exception
     * @return AbstractEntity
     */
    public function createEntity(string $classEntity, array $data): AbstractEntity
    {
        if (!is_subclass_of($classEntity, AbstractEntity::class)) {
            throw new Exception(sprintf("Class %s must be an instance of %s", $classEntity, AbstractEntity::class), 500);
        }

        if (isset($data['id'])) {
            throw new Exception('Unexpected field `id` to create');
        }

        /** @var AbstractEntity $sync */
        $sync = new $classEntity($this, $data);
        return $sync->save();
    }

    /**
     * @param string $classEntity
     * @param array $criteria
     *
     * @throws Exception
     * @return AbstractEntity
     */
    public function getEntity(string $classEntity, array $criteria): AbstractEntity
    {
        $entities = $this->getAllEntity($classEntity, $criteria, 1, 0);

        return $entities[0];
    }

    /**
     * @param string $classEntity
     * @param array $criteria
     * @param int|null $limit
     * @param int|null $offset
     *
     * @throws Exception
     * @return array
     */
    public function getAllEntity(string $classEntity, array $criteria, ?int $limit = null, ?int $offset = null): array
    {
        if (!is_subclass_of($classEntity, AbstractEntity::class)) {
            throw new Exception(sprintf("Class %s must be an instance of %s", $classEntity, AbstractEntity::class), 500);
        }

        $response = $this->makeRequest(
            "/" . $classEntity::$tableName . $this->handleCriteria($criteria),
            'GET',
            [],
            $limit,
            $offset
        );

        $this->getLogger()->info(
            sprintf('Entities of `%s` successfully received!', $classEntity::getEntityAlias()), [$criteria, $response]
        );

        $entities = [];
        foreach ($response as $item) {
            $entities[] = new $classEntity($this, $item);
        }

        return $entities;
    }

    /**
     * @param string $objectUrl
     * @param string $method
     * @param array $data
     * @param int|null $limit
     * @param int|null $offset
     *
     * @throws Exception
     * @return array
     */
    public function makeRequest(string $objectUrl, string $method, array $data = [], ?int $limit = null, ?int $offset = null): array
    {
        $parameters = [];

        if (null !== $limit) {
            $parameters['limit'] = $limit;
        }
        if (null !== $offset) {
            $parameters['offset'] = $offset;
        }

        return $this->client->makeRequest($objectUrl, $method, $data, $parameters);
    }

    /**
     * @param array $criteria
     *
     * @return string
     */
    private function handleCriteria(array $criteria): string
    {
        $criteriaString = '';
        foreach ($criteria as $key => $value) {
            $criteriaString .= "/$key/$value";
        }

        return $criteriaString;
    }
}
