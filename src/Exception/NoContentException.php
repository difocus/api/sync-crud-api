<?php


namespace ShopExpress\ShopCrmSyncClient\Exception;


/**
 * Class NoContentException
 * @package ShopExpress\ShopCrmSyncClient\Exception
 */
class NoContentException extends \Exception
{
}