<?php


namespace ShopExpress\ShopCrmSyncClient\Entity;


use ArrayAccess;
use Exception;
use ShopExpress\ShopCrmSyncClient\ApiClient;

/**
 * Class AbstractEntity
 * @package ShopExpress\ShopCrmSyncClient\Entity
 */
class AbstractEntity implements ArrayAccess
{
    /**
     * @var string
     */
    public static $tableName;

    /**
     * @var ApiClient
     */
    protected $apiClient;

    /**
     * @var array
     */
    protected $fields = [];

    /**
     * @var array
     */
    protected $requiredFields = [];

    /**
     * @param ApiClient $apiClient The api client
     * @param array $data The array of preset values
     */
    public function __construct(ApiClient $apiClient, array $data = [])
    {
        $this->apiClient = $apiClient;
        $this->fields = array_merge($this->fields, $data);
    }

    /**
     * Save a Sync if not exists by id or update otherwise.
     *
     * @throws Exception
     *
     * @return self
     */
    public function save(): self
    {
        try {
            $this->loadById($this->getId());
        } catch (Exception $e) {
            foreach ($this->requiredFields as $requiredField) {
                if (!isset($this->fields[$requiredField])) {
                    throw new Exception(sprintf("Required field `%s` isn't set for create!", $requiredField), 400);
                }
            }

            $response = $this->savePost($this->fields);
            $this->fields['id'] = $response['success']['id'];

            $this->apiClient->getLogger()->info(
                $this->getEntityAlias() . ' successfully created!', [$this->fields, $response]
            );

            return $this;
        }

        foreach ($this->requiredFields as $requiredField) {
            if (isset($this->fields[$requiredField]) && empty($this->fields[$requiredField])) {
                throw new Exception(sprintf("Required field `%s` can't be empty for update!", $requiredField), 400);
            }
        }

        $response = $this->savePut($this->fields);

        $this->apiClient->getLogger()->info(
            $this->getEntityAlias() . ' successfully updated!', [$this->fields, $response]
        );

        return $this;
    }

    /**
     * @param int $id
     *
     * @throws Exception
     *
     * @return self
     */
    public function loadById(int $id): self
    {
        $response = $this->apiClient->makeRequest(
            "/" . static::$tableName . "/id/$id",
            'GET'
        );

        $this->apiClient->getLogger()->info($this->getEntityAlias() . ' successfully received!', [$id, $response]);

        // перезаписываем существующие значения на новые, если были указаны
        $this->fields = array_merge($response[0], $this->fields);

        return $this;
    }

    /**
     * @return bool|int|string
     */
    public static function getEntityAlias()
    {
        if ($pos = strrpos(get_called_class(), '\\')) {
            return substr(get_called_class(), $pos + 1);
        }
        return $pos;
    }

    /**
     * @throws Exception
     */
    public function getId(): int
    {
        if (!isset($this->fields['id'])) {
            throw new Exception('Required field `id` not specified!');
        }

        return $this->fields['id'];
    }

    /**
     * @param array $fields
     *
     * @throws Exception
     * @return array
     */
    protected function savePost(array $fields): array
    {
        $response = $this->apiClient->makeRequest(
            "/" . static::$tableName,
            'POST',
            $fields
        );

        return $response;
    }

    /**
     * @param array $fields
     *
     * @throws Exception
     * @return array
     */
    protected function savePut(array $fields): array
    {
        $response = $this->apiClient->makeRequest(
            "/" . static::$tableName . "/" . $this->getId(),
            'PUT',
            $fields
        );

        return $response;
    }

    /**
     * @throws Exception
     * @return bool
     */
    public function delete(): bool
    {
        $response = $this->apiClient->makeRequest(
            "/" . static::$tableName . "/{$this->getId()}",
            'DELETE'
        );

        $this->apiClient->getLogger()->info(
            sprintf($this->getEntityAlias() . ' successfully deleted!'), [$this->fields, $response]
        );

        return true;
    }

    /**
     * Set a value to container.
     *
     * @param mixed $offset The offset
     * @param mixed $value The value
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->fields[] = $value;
        } else {
            $this->fields[$offset] = $value;
        }
    }

    /**
     * Isset a value in container.
     *
     * @param mixed $offset The offset
     *
     * @return mixed
     */
    public function offsetExists($offset)
    {
        return isset($this->fields[$offset]);
    }

    /**
     * Unset a value from container.
     *
     * @param mixed $offset The offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->fields[$offset]);
    }

    /**
     * Gets a value from container.
     *
     * @param mixed $offset The offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->fields[$offset]) ? $this->fields[$offset] : null;
    }
}