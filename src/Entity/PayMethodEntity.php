<?php


namespace ShopExpress\ShopCrmSyncClient\Entity;


/**
 * Class PayMethodEntity
 * @package ShopExpress\ShopCrmSyncClient\Entity
 */
class PayMethodEntity extends AbstractEntity
{
    /**
     * @var string
     */
    public static $tableName = 'pay_methods';

    /**
     * @var array
     */
    protected $requiredFields = [
        'sync_id',
    ];
}