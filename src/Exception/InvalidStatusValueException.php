<?php


namespace ShopExpress\ShopCrmSyncClient\Exception;


/**
 * Class InvalidStatusValueException
 * @package ShopExpress\ShopCrmSyncClient\Exception
 */
class InvalidStatusValueException extends \Exception
{
    protected $oldStatus;

    protected $newStatus;

    /**
     * @return mixed
     */
    public function getOldStatus()
    {
        return $this->oldStatus;
    }

    /**
     * @param mixed $oldStatus
     *
     * @return self
     */
    public function setOldStatus($oldStatus): self
    {
        $this->oldStatus = $oldStatus;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNewStatus()
    {
        return $this->newStatus;
    }

    /**
     * @param mixed $newStatus
     *
     * @return self
     */
    public function setNewStatus($newStatus): self
    {
        $this->newStatus = $newStatus;
        return $this;
    }
}