<?php


namespace ShopExpress\ShopCrmSyncClient\Entity;


/**
 * Class OrderStatusEntity
 * @package ShopExpress\ShopCrmSyncClient\Entity
 */
class OrderStatusEntity extends AbstractEntity
{
    /**
     * @var string
     */
    public static $tableName = 'order_statuses';

    /**
     * @var array
     */
    protected $requiredFields = [
        'sync_id',
    ];
}