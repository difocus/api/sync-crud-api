<?php

namespace ShopExpress\ShopCrmSyncClient\Injector;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Trait LoggerInjector
 * @package ShopExpress\ShopCrmSyncClient\Injector
 */
trait LoggerInjector
{
    /**
     * @var
     */
    private $logger;

    /**
     * @return NullLogger
     */
    public function getLogger()
    {
        if (!$this->logger) {
            $this->logger = new NullLogger();
        }
        return $this->logger;
    }

    /**
     * @param LoggerInterface $logger
     *
     * @return $this
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
        return $this;
    }
}
