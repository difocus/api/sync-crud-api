<?php

namespace ShopExpress\ShopCrmSyncClient\Entity;

use Exception;
use ShopExpress\ShopCrmSyncClient\Exception\InvalidStatusValueException;

/**
 * Class Sync
 * @package ShopExpress\ShopCrmSyncClient\Entity
 */
class SyncEntity extends AbstractEntity
{
    /**
     * @var string
     */
    public static $tableName = 'sync';

    const INSTALL_STATUS = 'install';
    const UNINSTALL_STATUS = 'uninstall';
    const ACTIVE_STATUS = 'active';
    const INACTIVE_STATUS = 'inactive';

    const STATUS_RESTRICTION_MESSAGE = 'The `%s` status can only be changed to the next set of statuses `%s`';

    /**
     * @var array
     */
    protected $requiredFields = [
        'user_id',
        'shop_type',
        'crm_type',
        'shop_url',
        'shop_apikey',
    ];

    /**
     * @param array $fields
     *
     * @throws Exception
     * @return array
     */
    protected function savePost(array $fields): array
    {
        $this->fields['status'] = self::INSTALL_STATUS;
        $this->fields['created_at'] = date("Y-m-d H:i:s");

        return parent::savePost($this->fields);
    }

    /**
     * @throws Exception
     * @return bool
     */
    public function delete(): bool
    {
        $this->fields['status'] = self::UNINSTALL_STATUS;

        $response = $this->apiClient->makeRequest(
            "/sync/{$this->getId()}",
            'PUT',
            $this->fields
        );

        $this->apiClient->getLogger()->info('Sync successfully added to queue for uninstall!', [$this->fields, $response]);

        return true;
    }

    /**
     * @param int|null $limit
     * @param int|null $offset
     *
     * @throws Exception
     * @return array
     */
    public function getSettings(?int $limit = null, ?int $offset = null): array
    {
        return $this->apiClient->getAllEntity(
            SyncSettingEntity::CLASS, ["sync_id" => $this->getId()], $limit, $offset
        );
    }

    /**
     * @param int|null $limit
     * @param int|null $offset
     *
     * @throws Exception
     * @return array
     */
    public function getDeliveryTypes(?int $limit = null, ?int $offset = null): array
    {
        return $this->apiClient->getAllEntity(
            DeliveryTypeEntity::CLASS, ["sync_id" => $this->getId()], $limit, $offset
        );
    }

    /**
     * @param int|null $limit
     * @param int|null $offset
     *
     * @throws Exception
     * @return array
     */
    public function getOrderPayStatus(?int $limit = null, ?int $offset = null): array
    {
        return $this->apiClient->getAllEntity(
            OrderPayStatusEntity::CLASS, ["sync_id" => $this->getId()], $limit, $offset
        );
    }

    /**
     * @param int|null $limit
     * @param int|null $offset
     *
     * @throws Exception
     * @return array
     */
    public function getOrderStatus(?int $limit = null, ?int $offset = null): array
    {
        return $this->apiClient->getAllEntity(
            OrderStatusEntity::CLASS, ["sync_id" => $this->getId()], $limit, $offset
        );
    }

    /**
     * @param int|null $limit
     * @param int|null $offset
     *
     * @throws Exception
     * @return array
     */
    public function getPayMethods(?int $limit = null, ?int $offset = null): array
    {
        return $this->apiClient->getAllEntity(
            PayMethodEntity::CLASS, ["sync_id" => $this->getId()], $limit, $offset
        );
    }

    /**
     * @param int|null $limit
     * @param int|null $offset
     *
     * @throws Exception
     * @return array
     */
    public function getProblems(?int $limit = null, ?int $offset = null): array
    {
        return $this->apiClient->getAllEntity(
            ProblemEntity::CLASS, ["sync_id" => $this->getId()], $limit, $offset
        );
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     *
     * @throws Exception
     */
    public function offsetSet($offset, $value)
    {
        if ($offset == 'status') {
            if (!in_array($value, [self::INSTALL_STATUS, self::UNINSTALL_STATUS, self::ACTIVE_STATUS, self::INACTIVE_STATUS])) {
                throw (new InvalidStatusValueException('Invalid value of field `status`'))
                    ->setNewStatus($value)->setOldStatus($this->fields['status']);
            }

            if (isset($this->fields['status'])) {
                if ($this->fields['status'] == self::INSTALL_STATUS
                    && !in_array($value, [self::INSTALL_STATUS, self::UNINSTALL_STATUS])
                ) {
                    throw (new InvalidStatusValueException(
                        sprintf(
                            self::STATUS_RESTRICTION_MESSAGE,
                            self::INSTALL_STATUS,
                            join(', ', [self::INSTALL_STATUS, self::UNINSTALL_STATUS])
                        )
                    ))->setNewStatus($value)->setOldStatus($this->fields['status']);
                }

                if ($value == self::INSTALL_STATUS
                    && in_array($this->fields['status'], [self::UNINSTALL_STATUS, self::ACTIVE_STATUS, self::INACTIVE_STATUS])
                ) {
                    throw (new InvalidStatusValueException(
                        sprintf(self::STATUS_RESTRICTION_MESSAGE, self::INSTALL_STATUS, self::INSTALL_STATUS)
                    ))->setNewStatus($value)->setOldStatus($this->fields['status']);
                }
            }
        }

        parent::offsetSet($offset, $value);
    }
}
