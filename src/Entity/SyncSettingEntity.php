<?php


namespace ShopExpress\ShopCrmSyncClient\Entity;


/**
 * Class SyncSettingEntity
 * @package ShopExpress\ShopCrmSyncClient\Entity
 */
class SyncSettingEntity extends AbstractEntity
{
    /**
     * @var string
     */
    public static $tableName = 'sync_settings';

    /**
     * @var array
     */
    protected $requiredFields = [
        'name',
        'sync_id',
    ];
}