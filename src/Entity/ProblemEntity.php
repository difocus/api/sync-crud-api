<?php


namespace ShopExpress\ShopCrmSyncClient\Entity;


/**
 * Class ProblemEntity
 * @package ShopExpress\ShopCrmSyncClient\Entity
 */
class ProblemEntity extends AbstractEntity
{
    /**
     * @var string
     */
    public static $tableName = 'problems';

    /**
     * @var array
     */
    protected $requiredFields = [
        'sync_id',
        'decision',
    ];
}