<?php


namespace ShopExpress\ShopCrmSyncClient\Http;


use Exception;
use ShopExpress\ShopCrmSyncClient\Exception\NoContentException;

/**
 * Class Client
 * @package ShopExpress\ShopCrmSyncClient\Http
 */
class Client
{
    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @var array
     */
    private $defaultParameters;

    /**
     * @param string $baseUrl The base url
     * @param array $defaultParameters
     */
    public function __construct(string $baseUrl, array $defaultParameters = [])
    {
        $this->baseUrl = trim($baseUrl, '/');
        $this->defaultParameters = $defaultParameters;
    }

    /**
     * Gets the base url.
     *
     * @return string The base url.
     */
    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    /**
     * Makes a request.
     *
     * @param string $objectUrl The object url
     * @param string $method The method
     * @param array $data The data
     * @param array $parameters
     *
     * @throws Exception
     * @return array
     */
    public function makeRequest(string $objectUrl, string $method, array $data = [], array $parameters = []): array
    {
        $ch = curl_init();

        $opts = [
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_USERAGENT => 'dicms-api-php-beta-0.2',
        ];

        $parameters = array_merge($this->defaultParameters, $parameters);

        switch ($method) {
            case 'GET':
                break;
            case 'PUT':
            case 'POST':
            case 'PATCH':
                $opts[CURLOPT_CUSTOMREQUEST] = $method;
                $opts[CURLOPT_RETURNTRANSFER] = true;
                $opts[CURLOPT_POSTFIELDS] = http_build_query($data);
                $opts[CURLOPT_HTTPHEADER][] = 'Content-Length: ' . strlen($opts[CURLOPT_POSTFIELDS]);
                break;
            case 'DELETE':
                $opts[CURLOPT_CUSTOMREQUEST] = $method;
                break;
            default:
                throw new Exception(sprintf('Unexpected method `%s`', $method));
        }

        $objectUrl = ltrim(str_replace($this->baseUrl, '', $objectUrl), '/');
        $opts[CURLOPT_URL] = $this->baseUrl . '/' . $objectUrl . '?' . http_build_query($parameters);

        curl_setopt_array($ch, $opts);
        $resultBody = curl_exec($ch);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $errno = curl_errno($ch);
        $error = curl_error($ch);

        curl_close($ch);

        if ($resultBody === false) {
            throw new Exception(sprintf('Curl error: %s (%s)', $errno, $error));
        }

        $response = json_decode($resultBody, true);

        if (json_last_error() != JSON_ERROR_NONE) {
            throw new Exception(json_last_error_msg(), 204);
        }

        if (isset($response['error'])) {
            if ($response['error'] == 'No Content') {
                throw new NoContentException($response['error']['message']);
            }
            throw new Exception($response['error']['message'], 204);
        }

        return $response;
    }
}